# https://br.advfn.com/bolsa-de-valores/bovespa/"+codes[i]+"/cotacao

print(":: Python Script START ::")

import datetime
from requests_html import HTMLSession

session = HTMLSession()
base_url = 'https://br.advfn.com/bolsa-de-valores/bovespa/'
#stocksCodes = ['CSNA3F', 'MOVI3F', 'PETR4F']
stocksCodes = [
	{'code':'CSNA3F', 'buy':15.57, 'stop':13.75},
	{'code':'MOVI3F', 'buy':12.27, 'stop':10.67},
	{'code':'ITSA4F', 'buy':0, 'stop':0}
]

today = datetime.datetime.now()

def getHttpPage(url):
	return session.get(url)

def getElement(selector, page):
	return page.html.find(selector, first=True)

def getElementText(element):
	return element.text

def getStockValue(element):
	if ( element.text == '-' ):
		return 0

	return float(element.text.replace(",", "."))

def getCurrentPrice(stockCode):
	url = base_url + stockCode+'/cotacao'
	page = getHttpPage(url)
	currentPrice = getElement('#quoteElementPiece6', page)
	minPrice = getElement('#quoteElementPiece8', page)
	maxPrice = getElement('#quoteElementPiece7', page)
	lastTime = getElement('#quoteElementPiece11', page)

	stock = {
		#'date': {'year': today.year, 'month':today.month, 'day':today.day},
		'lastTime' : getElementText(lastTime),
		'currentPrice' : getStockValue(currentPrice),
		'minPrice' : getStockValue(minPrice),
		'maxPrice' : getStockValue(maxPrice)
	}

	return stock

def run():
	for stock in stocksCodes:
		currentPrice = getCurrentPrice(stock['code'])['currentPrice']
		info = {
			'code': stock['code'],
			'current_price': currentPrice,
			'buy_price': stock['buy'],
			'diff':  (currentPrice - stock['buy'])*10,
			'stop_price': stock['stop']
		}

		print(getCurrentPrice(stock['code']))

run()

print(":: Python Script END ::")