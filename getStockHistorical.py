from requests_html import HTMLSession

session = HTMLSession()

stocksCodes = ['CSNA3F', 'MOVI3F', 'PETR4F']

def run():
	data = getData()
	print(data)


def getData():
	stockList = []

	url = 'http://cotacoes.economia.uol.com.br/acao/cotacoes-historicas.html?codigo=CSNA3.SA&beginDay=1&beginMonth=1&beginYear=2018&endDay=30&endMonth=3&endYear=2019&size=500&page=1&period='
	page = session.get(url)
	table = page.html.find('table#tblInterday', first=True)
	print(table)

	labels = table.find('thead tr th')
	valuesList = table.find('tbody tr')

	stockData = {
		'code' : 'CSNA3',
		'values' : []
	}

	for  item in valuesList: 
		value = {
			'date' : item.find('td')[0].text,
			'price' : float(item.find('td')[1].text.replace(',','.')),
			'min' : float(item.find('td')[2].text.replace(',','.')),
			'max' : float(item.find('td')[3].text.replace(',','.')),
			'variation_value' : float(item.find('td')[4].text.replace(',','.')),
			'variation_percent' : item.find('td')[5].text,
			'volume' : int(item.find('td')[6].text.replace('.',''))
		}

		stockData['values'].append(value)

	return stockData


run()