from requests_html import HTMLSession

session = HTMLSession()

def run():
	stockList = getList()
	showStockList(stockList)


def getList():
	stockList = []

	url = 'http://cotacoes.economia.uol.com.br/acoes-bovespa.html?exchangeCode=.BVSP&size=3000'
	page = session.get(url)
	codeList = page.html.find('ul.acoes-a-z li')
	for  item in codeList: 
		stockList.append( item.find('span')[1].text.replace('.SA', '') )

	return stockList



def showStockList(stockList):
	print(stockList);



run()