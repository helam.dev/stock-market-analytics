FROM python:3.6

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -s python-pip 
RUN pip install pipenv && pipenv install requests-html

COPY ./init.sh /usr/src/app/init.sh